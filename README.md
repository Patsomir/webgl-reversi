# WebGL Reversi

The well known game Reversi implemented with low level WebGL. It has Japanese letters made with bezier curves for aesthetics.

Use the left mouse button to place disks.
Use the right mouse button to change perspective.

![screenshot](https://gitlab.com/Patsomir/webgl-reversi/raw/master/screenshots/perspective-5.jpg)